import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import { Button } from './Button';
import './Navbar.css';

function Navbar() {
    // state refering to "menu-navbar icon" (hamburber-menu vs. X) keeping the information on the current icon state
    const [click, setClick] = useState (false);

    const [button, setButton] = useState (true);
    // function that simply change the click state (displayed nav-menu icon) from true to false and oposite (from hamburger-menu to X)
    const handleClick = () => setClick(!click);
    // 
    const closeMobileMenu = () => setClick(false);
    //  function that is going to display button depending on the screen size
    const showButton = () => {
        if (window.innerWidth <= 960) {
            setButton(false)
        } else {
            setButton(true)
        }
    };
    // useEffect with a [] argument makes that the showButton is displayed only at the first render and never when reload the page
    useEffect(() => {
        showButton()
    }, []);

    // eventListener added to the window to activate showButton function whenever  the window is resized
    window.addEventListener('resize', showButton);

    return (
        <>
            <nav className="navbar">
                <div className="navbar-container">
                    {/* eventListener added to execute closeMobileMenu function when logo clicked */}
                    <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
                        TRVL <i class="fab fa-typo3"></i>
                    </Link>
                    {/* eventListener added to lissen and invoke handleClick function defined in the top of the Navbar function*/}
                    <div className="menu-icon" onClick={handleClick}>
                        {/* turnary expression: if click the value (logo icon), then change it from fas fa-times to fas fa-bar and back (toggle) */}
                        <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
                    </div>
                    {/* ul className turnary expression hides nav menu after any of its ul elements is clicked */}
                    <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                        <li className="nav-item">
                            <Link to='/' className='nav-links' onClick={closeMobileMenu} >
                            Home
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to='/services' className='nav-links' onClick={closeMobileMenu} >
                            Services
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to='/products' className='nav-links' onClick={closeMobileMenu} >
                            Products
                            </Link>
                        </li>
                        <li className="nav-links-mobile">
                            <Link to='/sign-up' className='nav-links' onClick={closeMobileMenu} >
                            Sign Up
                            </Link>
                        </li>
                    </ul>
                    {button && <Button buttonStyle='btn--outline'>SIGN UP</Button>}
                </div>
            </nav>
        </>
    );
}

export default Navbar;