// This is a default reusable button factory component

import React from 'react';
import './Button.css';
import { Link } from 'react-router-dom';

// Button available classes incl. the default one 
const STYLES = ['btn--primary', 'btn--outline']
const SIZES = ['btn--medium', 'btn--large'];

// export before exports this const directly from the file
export const Button = ({
    children, 
    type, 
    onClick, 
    buttonStyle, 
    buttonSize
}) => {
    // const below checkes if the button component have a button style directly attached, then if it is true it applies it whatever the button style has been directly attached, but if it is not true, function set the default style value that is the first option of the styles array created above in "const STYLES" (btn--primary).
    const checkButtonStyle = STYLES.includes(buttonStyle) ? buttonStyle: STYLES[0];
    // const below works as above one in the area of button size
    const checkButtonSize = SIZES.includes(buttonSize) ? buttonSize: SIZES[0];

    return (
        <Link to='./sign-up' className='btn-mobile'>
            <button className={`btn ${checkButtonStyle} ${checkButtonSize}`}
            onClick={onClick}
            type={type}
            >
                {/* children oznacza e funkcja weźmie wartość przycisku wpisaną ręcznie przy jego tworzeniu */}
                {children}
            </button>
        </Link>
    )
}; 